package thread.demo;

class Thrd1 implements Runnable {
	Thread t = new Thread(this);

	Thrd1(String s) {

		t.setName(s);
		t.start();

	}

	@Override
	public void run() {
		for (int i = 0; i <= 10; i++) {

			// System.out.println(t.getName() + " : " + i + " :" +
			// t.getPriority());
			System.out.println(t.getName() + " :" + i);
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}

	}

}

public class ThreadDemo {

	public static void main(String[] args) throws InterruptedException {

		// System.out.println(Thread.currentThread().getPriority());
		Thrd1 t1 = new Thrd1("Java");
		t1.t.join();
		Thrd1 t2 = new Thrd1("Python");
		t2.t.join();
		Thrd1 t3 = new Thrd1("Csahrp");
		t3.t.join();
		System.out.println("Finish");

		// t1.t.setName("Java");
		// t1.t.start();
		// t1.t.setPriority(10);
		// t1.t.setDaemon(true);
		//
		// t2.t.setName("Python");
		// t2.t.start();
		// t2.t.join();
		//
		// t3.t.setName("Csharp");
		// // t3.t.start();
		// t3.t.join();

	}

}
