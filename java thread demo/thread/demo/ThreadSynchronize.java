package thread.demo;

import java.util.Scanner;

class BookSeat {

	int totalSeat = 10;

	synchronized void book(int bookSeat) {

		if (totalSeat >= bookSeat) {
			System.out.println("Your seats book successfully : " + bookSeat);
			totalSeat = totalSeat - bookSeat;
			System.out.println("Remaining seat is : " + totalSeat);

		} else {

			System.out.println("Seat is fully booked.");

		}

	}

}

class Booking1 extends Thread {
	BookSeat booking;
	int bookSeat;

	Booking1(BookSeat booking, int bookSeat) {
		this.booking = booking;
		this.bookSeat = bookSeat;
	}

	public void run() {
		booking.book(bookSeat);
	}

}

class Booking2 extends Thread {
	BookSeat booking;
	int bookSeat;

	Booking2(BookSeat booking, int bookSeat) {
		this.booking = booking;
		this.bookSeat = bookSeat;

	}

	public void run() {
		booking.book(bookSeat);
	}
}

public class ThreadSynchronize {

	public static void main(String[] args) throws InterruptedException {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter your number of seat required?");

		BookSeat bs = new BookSeat();

		while (bs.totalSeat != 0) {
			int bookSeat = scanner.nextInt();

			Booking1 b1 = new Booking1(bs, bookSeat);
			b1.start();
			b1.join();

			int bookSeat1 = scanner.nextInt();
			Booking1 b3 = new Booking1(bs, bookSeat1);
			b3.start();
			b3.join(1000);

			int bookSeat2 = scanner.nextInt();
			Booking2 b2 = new Booking2(bs, bookSeat2);
			b2.start();
			b2.join(2000);

			int bookSeat3 = scanner.nextInt();
			Booking2 b4 = new Booking2(bs, bookSeat3);
			b4.start();
			b4.join(3000);

		}
		scanner.close();

	}
}
