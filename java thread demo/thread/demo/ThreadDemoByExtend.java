package thread.demo;

class Demo extends Thread {

	@Override
	public void run() {

		if (Thread.currentThread().isDaemon()) {

			System.out.println("Deamon thread");

			for (int i = 0; i <= 10; i++) {

				System.out.println(getName() + " : " + i);
				System.out.println("priority : " + Thread.currentThread().getPriority());

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		} else {
			System.out.println("this is not daemon Thread");
		}
	}
}

public class ThreadDemoByExtend {
	public static void main(String[] args) throws InterruptedException {
		//System.out.println(Thread.currentThread().getName() + " : " + Thread.currentThread().getPriority());
		Demo d = new Demo();
		d.setDaemon(true);
		d.start();
		d.setName("java");
		d.setPriority(4);
		d.join();

		Demo d1 = new Demo();
		//d1.setDaemon(true);
		d1.start();
		d1.setName("python");
		d1.setPriority(10);
	}

}
