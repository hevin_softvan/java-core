package demo.date.api;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class DateTimeDemo {

	public static void main(String[] args) {

		LocalDate localDate = LocalDate.now();
		System.out.println(localDate);

		LocalTime localTime = LocalTime.now();
		System.out.println(localTime);

		LocalDateTime localDateTime = LocalDateTime.now();
		System.out.println(localDateTime);

		int dayOfMonth = localDate.getDayOfMonth();
		System.out.println(dayOfMonth);

		DayOfWeek dayOfWeek = localDate.getDayOfWeek();
		System.out.println(dayOfWeek);

		Month month = localDate.getMonth();
		System.out.println(month);

		int year = localDate.getYear();
		System.out.println(year);

		int hour = localTime.getHour();
		int minute = localTime.getMinute();
		int second = localTime.getSecond();

		System.out.println(hour + " : " + minute + " : " + second);

		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd - MMM - yyyy, HH - mm - ss");
		String format = localDateTime.format(dateTimeFormatter);
		System.out.println(format);

	}
}
