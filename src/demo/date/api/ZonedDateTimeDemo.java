package demo.date.api;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ZonedDateTimeDemo {

	public static void main(String[] args) {

		// to get the current zone
		ZonedDateTime date = ZonedDateTime.now();
		System.out.println(date);

		// get zone
		System.out.println(date.getZone());

		ZoneId Calcutta = ZoneId.of("Asia/Calcutta");

		// get time Zone of specific place
		ZonedDateTime zonedDateTime = date.withZoneSameInstant(Calcutta);
		System.out.println(zonedDateTime);

		DateTimeFormatter formate = DateTimeFormatter.ofPattern("dd/MMM/yyyy, HH-mm-ss");
		String format = date.format(formate);
		System.out.println(format);

		// get available all zone id
		for (String s : ZoneId.getAvailableZoneIds()) {
			System.out.println(s);
		}
	}
}
