package demo.filterAndForEach;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FilterAndForEach {

	public static void main(String[] args) {

		List<String> player = new ArrayList<String>();
		player.add("Dhoni");
		player.add("Rahul");
		player.add("Kohli");
		player.add("Rohit");
		player.add("Jaddu");

		// iterate through foreach method
		for (Object p : player) {
			System.out.println("player list is : " + p);
		}

		System.out.println("-------------------------------------------------------");

		// iterate through stream api forEach method
		player.stream().forEach(t -> {
			System.out.println(t);
		});

		System.out.println("-------------------------------------------------------");
		// filter method using foreach stream api
		player.stream().filter(t -> t.equals("Dhoni")).forEach(t -> {
			System.out.println(t);
		});

		System.out.println("-------------------------------------------------------");

		// sorted using sort method
		Collections.sort(player); // asscending
		System.out.println("sorted list: " + player);

		System.out.println("-------------------------------------------------------");
		Collections.reverse(player); // Discending
		System.out.println("sorted list: " + player);

		System.out.println("-------------------------------------------------------");
		player.stream().sorted().forEach(s -> {
			System.out.println(s);
		});

		System.out.println("-------------------------------------------------------");
		player.stream().sorted(Comparator.reverseOrder()).forEach(s -> {
			System.out.println(s);
		});
	}
}
