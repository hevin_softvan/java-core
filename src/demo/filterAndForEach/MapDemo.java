package demo.filterAndForEach;

import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.*;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {

		Map<Integer, String> player = new HashMap<>();

		player.put(45, "Rohit");
		player.put(18, "Kohli");
		player.put(07, "Dhoni");
		player.put(12, "Jadeja");
		player.put(01, "Rahul");
		player.put(05, "Raina");

		// using iterator
		// Set s = player.keySet();
		// Iterator i = s.iterator();
		// while (i.hasNext()) {
		// System.out.println(i.next());
		// }

		System.out.println("__using forEach______");

		for (Object p : player.entrySet()) {
			System.out.println(p);

		}
		System.out.println("---------------------------------------------------------------------------");

		player.forEach((k, v) -> {
			System.out.println("get Key: " + k + "get value :" + v);

		});
		System.out.println("---------------------------------------------------------------------------");
		// using lambda function

		player.entrySet().stream().forEach(t -> {
			System.out.println("player list is: " + t);

		});
		System.out.println("---------------------------------------------------------------------------");

		// use filter to find key value is 12
		player.entrySet().stream().filter(k -> k.getValue().equals("Jadeja")).forEach(obj -> {
			System.out.println("player list is: " + obj);

		});
		System.out.println("---------------------------------------------------------------------------");

		// convert map into list by sort to map.
		List<java.util.Map.Entry<Integer, String>> playerList = new ArrayList<>(player.entrySet());
		Collections.sort(playerList, (o1, o2) -> {
			return o1.getKey().compareTo(o2.getKey());
		});
		System.out.println(playerList);
		/*
		 * for (java.util.Map.Entry<Integer, String> en : playerList) {
		 * System.out.println(en.getKey() + " : " + en.getValue()); }
		 */
		System.out.println("---------------------------------------------------------------------------");
		// using stream api
		player.entrySet().stream().sorted(Comparator.comparing(emp -> emp.getKey())).forEach(System.out::println);
		System.out.println("---------------------------------------------------------------------------");
		player.entrySet().stream().sorted(Comparator.comparing(emp -> emp.getValue())).forEach(System.out::println);
		System.out.println("---------------------------------------------------------------------------");

	}

}