package demo.mapAndReduce;

import java.util.*;

public class MapAndReduce {
	public static void main(String[] args) {

		List<String> str = Arrays.asList("Java", "Python", "DotNet", "JavaScript", "Go", "Swift");
		String longestword = str.stream().reduce((w1, w2) -> w1.length() > w2.length() ? w1 : w2).get();
		System.out.println(longestword);

		List<Integer> num = Arrays.asList(1, 5, 7, 9, 11, 13);

		/*
		 * int sum = 0; for (int no : num) {
		 * 
		 * sum = sum + no;
		 * 
		 * } System.out.println(sum);
		 */

		int sum = num.stream().mapToInt(i -> i).sum();
		System.out.println("sum is :" + sum);

		Integer sum1 = num.stream().reduce(0, (a, b) -> a + b);
		System.out.println("sum is :" + sum);

		// with method reference
		Optional<Integer> sum2 = num.stream().reduce(Integer::sum);
		System.out.println("sum is :" + sum2.get());

		Integer sum3 = num.stream().reduce(1, (a, b) -> a * b);
		System.out.println("Multiply is :" + sum3);

		Integer max = num.stream().reduce(0, (a, b) -> a > b ? a : b);
		System.out.println("max is :" + max);

		Integer min = num.stream().reduce(Integer::min).get();
		System.out.println("min is :" + min);
	}
}
