package demo.lambdaExemple;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BookServices {

	public List<Book> getBooks() {
		List<Book> books = BookDao.getBooks();
		/* sort using only lambda function */
		Collections.sort(books, (o1, o2) -> (o1.getBookName().compareTo(o2.getBookName())));
		return books;

	}

	public void sort() {
		/* this is sort method using stream */
		List<Book> books = BookDao.getBooks();
		books.stream().sorted(Comparator.comparing(Book::getBookName)).forEach(System.out::println);

		/* get book data only from Book Object using map */
		List<String> bookName = books.stream().map(Book::getBookName).collect(Collectors.toList());
		System.out.println(bookName);

		/*
		 * get longestBookName from list of book getting by map and reduce
		 * function
		 */
		Book longBookName = books.stream()
				.reduce((b1, b2) -> b1.getBookName().length() > b2.getBookName().length() ? b1 : b2).get();
		System.out.println(longBookName);

	}

	public static void main(String[] args) {

		BookServices book = new BookServices();
		System.out.println(book.getBooks());
		book.sort();

	}

}
