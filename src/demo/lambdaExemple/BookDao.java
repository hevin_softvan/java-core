package demo.lambdaExemple;

import java.util.ArrayList;
import java.util.List;


//create list of book object.
public class BookDao {

	public static List<Book> getBooks() {

		List<Book> books = new ArrayList<Book>();
		books.add(new Book(1, "java"));
		books.add(new Book(2, "java advance"));
		books.add(new Book(3, "python"));
		books.add(new Book(4, "dotnet"));
		books.add(new Book(5, "csharp"));
		return books;

	}

}
