package demo.lambdaExemple;

import java.util.Collections;
import java.util.List;

//create bookservice to sort list of book by name by comparator method.
public class BookService {

	public List<Book> getSortedBooks() {
		List<Book> books = BookDao.getBooks();
		// Collections.sort(books, new BookComparator());
		// sorting book by name using lambda function
		Collections.sort(books, (o1, o2) -> (o1.getBookName().compareTo(o2.getBookName())));

		return books;
	}

	/*
	 * @Override public int compare(Book o1, Book o2) { return
	 * o1.getBookName().compareTo(o2.getBookName()); } }); return books; }
	 */
	public static void main(String[] args) {

		System.out.println("Books is sorting by name : " + new BookService().getSortedBooks());
	}

}
// here implements the comparator class.
// class BookComparator implements Comparator<Book> {
//
// @Override
// public int compare(Book o1, Book o2) {
// return o1.getBookName().compareTo(o2.getBookName());
// }
//
// }

/*
 * Books is sorting by name : [Book [id=5, bookName=csharp], Book [id=4,
 * bookName=dotnet], Book [id=1, bookName=java], Book [id=2, bookName=java
 * advance], Book [id=3, bookName=python]]
 * 
 */