package demo.lambdaExemple;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class MonthSorting {

	public static void main(String[] args) {

		List<String> months = new ArrayList<>();
		months.add("January");
		months.add("March");
		months.add("July");
		months.add("December");

		System.out.println(months);

	}

	class ComparatorMonth implements Comparator<String> {

		@Override
		public int compare(String o1, String o2) {
			SimpleDateFormat dt = new SimpleDateFormat("MMM");

			try {
				return dt.parse(o1).compareTo(dt.parse(o2));
			} catch (ParseException e) {

				e.printStackTrace();
			}
			return o1.compareTo(o2);
		}

	}

}
