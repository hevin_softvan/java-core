package demo.base64;

import java.util.Base64;

public class UrlBase64 {
	
	public static void main(String[] args) {
		
		String url = "www.hevinmulani.com";
		
		System.out.println("sample url : " +url);
	    String encodedUrl = Base64.getUrlEncoder().encodeToString(url.getBytes());
	    System.out.println("encodeed url : " +encodedUrl);
	    
	    
	    byte[] decode = Base64.getUrlDecoder().decode(encodedUrl);
	    String decodedUrl = new String(decode);
	    System.out.println("decoded url : "+decodedUrl);
		
	}
}
