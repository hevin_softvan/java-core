package demo.base64;

import java.util.Base64;

public class DemoBase64 {
	
	public static void main(String[] args) {
		
		String name = "Suman Sudani";
		
		System.out.println("Name : " + name);
		
		
		//here encode into Base64 formate
		
		String encodedName = Base64.getEncoder().encodeToString(name.getBytes());
		System.out.println("Encoded name : " +encodedName);
		
		
		//converted encodedName in to decodedName
		
		byte[] decode = Base64.getDecoder().decode(encodedName);
		
		String decodedName = new String(decode);
		System.out.println("Decoded name : " +decodedName);
	}

}
