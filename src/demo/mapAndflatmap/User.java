package demo.mapAndflatmap;

import java.util.List;
import java.util.Optional;

public class User {

	private int id;
	private String name;
	private String username;
	private List<String> mob;

	public User(int id, String name, String username, List<String> list) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.mob = list;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", username=" + username + ", mob=" + mob + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<String> getUsername() {
		return Optional.ofNullable(username);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getMob() {
		return mob;
	}

	public void setMob(List<String> mob) {
		this.mob = mob;
	}

}
