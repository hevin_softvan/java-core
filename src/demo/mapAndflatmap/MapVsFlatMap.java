package demo.mapAndflatmap;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MapVsFlatMap {

	public void map() {

		List<User> user = UserDao.getAll();

		/*
		 * Sorting by list using stream api user.stream().sorted((o1, o2) -> {
		 * return o1.getName().compareTo(o2.getName());
		 * }).forEach(System.out::println); System.out.println(
		 * "---------------------------------------------------------------------------"
		 * );
		 * 
		 * user.stream().sorted(Comparator.comparing(User::getId)).forEach(
		 * System.out::println);
		 */

		// get only email using map
		// here List<User> return List<String> --> data Transfer
		// mapping users -> users.getUsername() --> one to one mapping
		// here used optional method
		List<Optional<String>> emails = user.stream().map(users -> users.getUsername()).collect(Collectors.toList());
		System.out.println(Optional.ofNullable(emails).get());

		System.out.println("---------------------------------------------------------------------------");
		List<List<String>> mobile = user.stream().map(mob -> mob.getMob()).collect(Collectors.toList());
		System.out.println(mobile);

		System.out.println("---------------------------------------------------------------------------");
		// here List<User> return List<String> --> data Transfer
		// mapping users -> users.getUsername() --> one to many mapping
		List<String> phone = user.stream().flatMap(mob -> mob.getMob().stream()).collect(Collectors.toList());
		System.out.println(phone);

	}

	public static void main(String[] args) {
		MapVsFlatMap mf = new MapVsFlatMap();
		mf.map();
	}

}
