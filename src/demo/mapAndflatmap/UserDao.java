package demo.mapAndflatmap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDao {

	public static List<User> getAll() {

		List<User> user = new ArrayList<User>();

		user.add(new User(1, "Suman", "suman@gmail.com", Arrays.asList("1234", "4567")));
		user.add(new User(2, "Ronak", "ronak@gmial.com", Arrays.asList("2345", "5678")));
		user.add(new User(3, "Jenish", "jenish@gmial.com", Arrays.asList("3456", "6789")));
		user.add(new User(4, "Jay", "jay@gmial.com", Arrays.asList("9876", "4321")));
		return user;

	}

}
