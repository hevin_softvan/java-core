package demo.filterAndFunctionExample;

import java.util.Optional;

public class OptionalDemo {

	public static void main(String[] args) {
		String str = null;
		String name = "Java";

		Optional<String> optionalStr = Optional.ofNullable(str);
		System.out.println(optionalStr.orElse("no value is present"));

		Optional<String> optionalName = Optional.of(name);
		System.out.println(optionalName.orElse("Name not found!!"));
	}

}
