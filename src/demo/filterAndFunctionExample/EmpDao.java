package demo.filterAndFunctionExample;

import java.util.ArrayList;
import java.util.List;

public class EmpDao {

	public static List<Employee> getEmployee() {

		List<Employee> emp = new ArrayList<Employee>();
		emp.add(new Employee(1, "Suman", 20000, "Python"));
		emp.add(new Employee(2, "Ronak", 10000, "Java"));
		emp.add(new Employee(3, "Jay", 50000, "DotNet"));
		emp.add(new Employee(4, "Jenish", 80000, "JavaScript"));
		return emp;

	}

}
