package demo.filterAndFunctionExample;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.omg.Messaging.SyncScopeHelper;

public class EmpService {

	public List<Employee> taxPayee(String input) {

		/*
		 * if (input.equals("tax")) { return
		 * EmpDao.getEmployee().stream().filter(emp -> emp.getSalery() >
		 * 50000).collect(Collectors.toList()); } else { return
		 * EmpDao.getEmployee().stream().filter(emp -> emp.getSalery() <=
		 * 50000).collect(Collectors.toList()); }
		 */

		return (input.equals("tax"))
				? EmpDao.getEmployee().stream().filter(emp -> emp.getSalery() > 50000).collect(Collectors.toList())

				: EmpDao.getEmployee().stream().filter(emp -> emp.getSalery() <= 50000).collect(Collectors.toList());

	}

	// this methoid for sorting by employee salery
	public void sort() {
		List<Employee> employee = EmpDao.getEmployee();
		// Collections.sort(employee, new EmpComp());
		/*
		 * Collections.sort(employee, new Comparator<Employee>() {
		 * 
		 * @Override public int compare(Employee o1, Employee o2) {
		 * 
		 * return o2.getSalery().compareTo(o1.getSalery()); } });
		 * System.out.println(employee);
		 */

		// sort by lambada experssion
		/*
		 * Collections.sort(employee, (o1, o2) -> { return
		 * o1.getSalery().compareTo(o2.getSalery());
		 * 
		 * });
		 */

		// sort list using lambada function
		// employee.stream().sorted((o1, o2) -> {
		// return o1.getSalery().compareTo(o2.getSalery());
		// }).forEach(System.out::println);

		// sort using method reference
		employee.stream().sorted(Comparator.comparing(emp -> emp.getSalery())).forEach(System.out::println);
		System.out.println("----------------------------By Salary-----------------------------------------------");
		// sort by method redference
		employee.stream().sorted(Comparator.comparing(Employee::getSalery).reversed()).forEach(System.out::println);

		System.out.println("*******************************************************");

		/*
		 * here i used map reduce function to get average salery of employee
		 * which salery is more than 50000.
		 */
		double avgSalery = employee.stream().filter(emp -> emp.getSalery() > 30000).map(Employee::getSalery)
				.mapToDouble(i -> i).average().getAsDouble();
		System.out.println(avgSalery);

		System.out.println("*******************************************************");
		System.out.println("this is example of find shortest name of employee by map and reduce method. ");
		Employee shortestEmpName = employee.stream()
				.reduce((w1, w2) -> w1.getEmpName().length() < w2.getEmpName().length() ? w1 : w2).get();
		System.out.println(shortestEmpName);

		System.out.println("*******************************************************");
		System.out.println("this is example by only get employeename data from object of employee.");
		List<String> employeeName = employee.stream().map(Employee::getEmpName).collect(Collectors.toList());
		System.out.println(employeeName);
	}

	public static void main(String[] args) {
		EmpService empService = new EmpService();
		System.out.println(empService.taxPayee("no-tax"));
		System.out.println("---------------------------------------------------------------------------");
		empService.sort();

	}

	/*
	 * class EmpComp implements Comparator<Employee> {
	 * 
	 * @Override public int compare(Employee o1, Employee o2) {
	 * 
	 * return o2.getSalery().compareTo(o1.getSalery()); }
	 * 
	 * }
	 */

}
