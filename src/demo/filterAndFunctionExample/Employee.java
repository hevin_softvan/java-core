package demo.filterAndFunctionExample;

public class Employee {

	private Integer id;
	private String empName;
	private Integer salery;
	private String department;

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Integer getSalery() {
		return salery;
	}

	public void setSalery(Integer salery) {
		this.salery = salery;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Employee [empName=" + empName + ", salery=" + salery + ", id=" + id + ", department=" + department
				+ "]";
	}

	public Employee(Integer id, String empName, Integer salery, String department) {
		super();
		this.id = id;
		this.empName = empName;
		this.salery = salery;
		this.department = department;
	}

	public Employee() {
		super();

	}

}
