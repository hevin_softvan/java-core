package demo.parallel.stream;

import java.util.stream.IntStream;

import org.omg.PortableServer.Current;

public class ParrallelStreamDemo {

	public static void main(String[] args) {

		// long start = System.currentTimeMillis();
		// IntStream.range(1, 100).forEach(System.out::println);
		// long end = System.currentTimeMillis();
		//
		// System.out.println(" plain Stream took time : " + (end - start));
		//
		// long start1 = System.currentTimeMillis();
		// IntStream.range(1, 100).parallel().forEach(System.out::println);
		// long end1 = System.currentTimeMillis();
		//
		// System.out.println(" parallel Stream took time : " + (end1 -
		// start1));

		long start = System.currentTimeMillis();
		IntStream.range(1, 10)
				.forEach(x -> System.out.println("Plain thread  : " + Thread.currentThread().getName() + " : " + x));
		long end = System.currentTimeMillis();
		System.out.println("plain Stream : " + (end - start));
		long start1 = System.currentTimeMillis();
		IntStream.range(1, 10).parallel()
				.forEach(x -> System.out.println("Parallel thresd : " + Thread.currentThread().getName() + " : " + x));
		long end1 = System.currentTimeMillis();
		System.out.println("parallel Stream : " + (end1 - start1));
	}

}
