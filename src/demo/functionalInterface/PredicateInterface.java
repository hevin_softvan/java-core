package demo.functionalInterface;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PredicateInterface /* implements Predicate<Integer> */ {

	public static void main(String[] args) {

		/*
		 * Predicate<Integer> predicate = (t) -> { if (t % 2 == 0) { return
		 * true; } else { return false; }
		 * 
		 * };
		 */

		// System.out.println(predicate.test(89));
		// System.out.println(predicate.test(20));

		List<Integer> numList = Arrays.asList(10, 25, 37, 73, 89, 46);

		numList.stream().filter(t -> t % 2 == 0).forEach(t -> {
			System.out.println("list of even number is : " + t);
		});

	}
	/*
	 * public static void main(String[] args) { PredicateInterface pi = new
	 * PredicateInterface(); System.out.println(pi.test(89)); }
	 */
}
