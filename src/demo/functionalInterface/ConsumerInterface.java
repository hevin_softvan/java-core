package demo.functionalInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerInterface /* implements Consumer<Integer> */ {

	// @Override
	// public void accept(Integer t) {
	// System.out.println("Numers is :" + t);
	// }

	public static void main(String[] args) {

		/*
		 * ConsumerInterface ci = new ConsumerInterface(); ci.accept(20);
		 */

		/*
		 * Consumer<Integer> consumer = (t) -> {
		 * System.out.println("Numers is :" + t); }; consumer.accept(100);
		 */

		List<Integer> numList = Arrays.asList(1, 3, 4, 7, 9, 5, 8);

		numList.stream().forEach(t -> {
			System.out.println("Numers is :" + t);
		});

	}
}
