package demo.functionalInterface;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class SupplierInterface /* implements Supplier<String> */ {

	/*
	 * @Override public String get() {
	 * 
	 * return "This is supplier method."; }
	 * 
	 * public static void main(String[] args) { SupplierInterface si = new
	 * SupplierInterface(); System.out.println(si.get()); }
	 */

	public static void main(String[] args) {

		/*
		 * Supplier<String> supplier = () -> { return
		 * "this is supplier functional interface";
		 * 
		 * };
		 * 
		 * System.out.println(supplier.get());
		 */

		List<String> player = Arrays.asList("Dhoni", "Sachin", "Kohli", "Rahul", "sachin");

		System.out.println(player.stream().filter(t -> t.equals("Kohli")).findFirst().orElseGet(() -> "not found"));

	}

}
