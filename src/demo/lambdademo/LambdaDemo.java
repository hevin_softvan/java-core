package demo.lambdademo;

//functional interface : contain only one abstract method.
interface Calculator {

	// void main();

	// void sum(int number);

	int sum(int num1, int num2);
}

public class LambdaDemo {

	public static void main(String[] args) {

		/*
		 * this is basic function without parameter. Calculator calc = () -> {
		 * 
		 * System.out.println("Hello java, this is lambada function!!"); };
		 * 
		 * calc.main();
		 */

		/*
		 * Calculator calc = (number) -> {
		 * 
		 * System.out.println("Square of number is : " + number * number); };
		 * 
		 * calc.sum(10);
		 */

		Calculator calc = (num1, num2) -> {

			if (num1 > num2) {

				return (num1 - num2);
			} else {
				throw new RuntimeException("num1 should be higher than num2.");
			}

		};

		System.out.println(calc.sum(40, 20));

	}
}

/*
 * public class LambdaDemo implements Calculator { public static void
 * main(String[] args) {
 * 
 * LambdaTest l2 = new LambdaTest(); l2.main(); }
 * 
 * @Override public void main() { System.out.println("Hello java!!"); } }
 */
