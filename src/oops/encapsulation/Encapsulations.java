package oops.encapsulation;

public class Encapsulations {

	public static void main(String[] args) {

		Product obj = new Product();
		obj.setProductName("Shirts");
		obj.setProductPrice(500);

		// here i can not set price if boolean isAdmin is not true.
		System.out.println("product name : " + obj.getProductName() + " && product price : " + obj.getProductPrice());

	}

}

class Product {

	String productName;
	private int productPrice;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(int productPrice) {

		boolean isAdmin = true;
		if (isAdmin) {
			this.productPrice = productPrice;
		} else {
			System.out.println("only aadmin is set the product price.");
		}
	}

}