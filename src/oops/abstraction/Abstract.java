package oops.abstraction;

public class Abstract {

	public static void main(String[] args) {

		// Cannot instantiate the type Abstract Mobile
		// Mobile mobile = new Mobile();

		Samsung s = new Samsung();
		s.price = 100000L;
		System.out.println(s.price);
		s.isAvailable();
		s.manufacture();
		
		IPhone i = new IPhone();
		i.price = 70000L;
		System.out.println(i.price);
		i.isAvailable();
		i.manufacture();


	}
}

	class Samsung extends Mobile {

		@Override
		void isAvailable() {
			System.out.println("Samsung is not Available.");

		}

	}

	class IPhone extends Mobile {

		@Override
		void isAvailable() {
			System.out.println("iPhone is Available.");
		}

	}



abstract class Mobile {

	long price;

	abstract void isAvailable();

	void manufacture() {
		System.out.println("Manufacturing is Starting.......");
	}
}