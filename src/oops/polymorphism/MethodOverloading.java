//method overloading 
package oops.polymorphism;
//type-1:Same method and different Parameter.
class Animals {

	void sound() {
		System.out.println("meow....meow.....");
	}

	void sound(String name) {
		System.out.println(name + " : bow.....bow...");
	}

	void sound(String name1, String name2) {
		System.out.println(name1 + " : roar.....roar...");
		System.out.println(name2 + " : bawl.....bawl...");
	}
}

// type-2:Same method, same Parameter but different datatype
class Math {

	void sum(int num1, int num2) {
		int sum = num1 + num2;
		System.out.println("sum of two integer number : " + sum);
	}
    void sum(float num1f, float num2f) {
		float sum = num1f + num2f;
		System.out.println("sum of two float number : " + sum);
	}
    void sum(long num1, long num2) {
		long sum = num1 + num2;
		System.out.println("sum of two long number : " + sum);
	}

}

public class MethodOverloading {

	public static void main(String[] args) {

		System.out.println("type-1:Same method and different Parameter.");
		Animals a = new Animals();
		a.sound();
		a.sound("Dog");
		a.sound("Lion", "Cow");

		System.out.println("_____________________________");
		System.out.println("type-2 : Same method, same Parameter but different datatype");
		Math m = new Math();
		m.sum(50, 50);
		m.sum(10.5f, 20.5f);
		m.sum(100000, 200000);

	}

}
