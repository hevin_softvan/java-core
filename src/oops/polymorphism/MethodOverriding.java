package oops.polymorphism;

class Animal {

	void sound() {
		System.out.println("Cat Sound : Meow.....Meow");
	}

}

class Bird extends Animal {

	void sound() {
		/*
		 * if we call parent class method by child class we use super keyword.
		 */
		super.sound();
		System.out.println("Cuckoo Sound : kuhoo.....koohu");
	}

}

public class MethodOverriding {
	public static void main(String[] args) {

		// if we create parent class object then parent class method call.
		Animal a = new Animal();
		a.sound();

		/*
		 * if we create child class object then parent class method is
		 * overriding by child class method.
		 */
		Bird b = new Bird();
		b.sound();

		// here birds method is called.
		Animal a1 = new Bird();
		a1.sound();

	}

}
