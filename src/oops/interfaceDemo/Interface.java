package oops.interfaceDemo;

public class Interface {

	public static void main(String[] args) {

		Mobile mobile = new Mobile();
		mobile.brand();
		mobile.newBrand();

		Tablet tablet = new Tablet();
		tablet.brand();
		tablet.newBrand();
		tablet.model();

	}
}

interface Brand {

	String model = "Samsung";

	void brand();

}

interface NewBrand {

	String newModel = "Apple";

	void newBrand();

}

class Mobile implements Brand, NewBrand {

	@Override
	public void newBrand() {
		System.out.println("Brand : " + newModel);

	}

	@Override
	public void brand() {
		System.out.println("Brand : " + model);
	}

}

class Tablet extends Mobile {
	void model() {
		System.out.println("oppo");
	}
}
