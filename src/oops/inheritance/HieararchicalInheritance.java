/* hierarchical inheritance. more than one class are inheritance from super
 class.*/
package oops.inheritance;

class Wishing {
	void wish() {
		System.out.println("Have a nice day!!!!");
	}
}

class Saying extends Wishing {
	void say() {
		System.out.println("good morning");
	}
}

class Greeting extends Wishing {
	void greeting() {
		System.out.println("Happy birhday to you!!");
	}
}

public class HieararchicalInheritance {

	public static void main(String[] args) {

		Wishing h = new Wishing();
		h.wish();
		// h.say();
		// h.greeting();

		Saying h1 = new Saying();
		h1.say();
		// h1.greeting();
		h1.wish();

		Greeting h2 = new Greeting();
		h2.greeting();
		h2.wish();
		// h2.say();

	}

}
