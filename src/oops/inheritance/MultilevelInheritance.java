package oops.inheritance;

class Sum {
	int num1 = 10, num2 = 20;

	void sum() {

		int addition = num1 + num2;
		System.out.println("sum of to number : " + addition);

	}
}

class Prod extends Sum {
	void prod() {
		int multiply = num1 * num2;
		System.out.println("production of to number : " + multiply);

	}
}

class Sub extends Prod {
	void sub() {

		int minus = num1 - num2;
		System.out.println("subtraction of to number : " + minus);

	}
}

public class MultilevelInheritance {

	public static void main(String[] args) {

		Sum s = new Sum();
		s.sum();

		Prod p = new Prod();
		p.prod();
		p.sum();

		Sub s1 = new Sub();
		s1.sub();
		s1.prod();
		s1.sum();
		
		Sum s2 = new Prod();
		s2.sum();
		//s2.prod();
		//s2.sub();
		
		Sum s3 = new Sub();
		s3.sum();
      //s3.prod();
	  //s3.Sub();
		
		Prod p2 = new Sub();
		p2.prod();
		p2.sum();
		//p2.sub();
		
		

	}

}
