//Inheritance
//Here priority of declared varibale is :   1.Local varibale > 2.object called varible > 3.global variable

package oops.inheritance;

class Parent {

	// Global variable
	int num1 = 10, num2 = 20;

	void sum() {

		// Local variable
		int num1 = 5, num2 = 10;

		int sum = num1 + num2;
		System.out.println("sum of two number is: " + sum);
	}
}

class Child extends Parent {

	void prod() {

		// local varible
		int num1 = 2, num2 = 10;
		int prod = num1 * num2;
		System.out.println("production of two number is: " + prod);
	}
}

public class Test {

	public static void main(String[] args) {

		System.out.println("Parent class object called : ");

		// object create of parent class
		Parent p = new Parent();

		// value when object is call.
		p.num1 = 50;
		p.num2 = 20;
		p.sum();

		/*
		 * Here we can not access child class method to create an object of
		 * parent class.
		 */
		// p.prod();
		System.out.println("child class object called :");

		// object create of child class
		Child c = new Child();

		// value when object is call.
		c.num1 = 20;
		c.num2 = 30;
        c.prod();
		 /*Here we can access parent class method by object creation of child
 class*/
		c.sum();

		// cannot convert from Parent to Child
		// Child c1 = new Parent();
		// c.sum();
		// c.prod();

		Parent p1 = new Child();
		p1.sum();
        // p.prod();
	}

}
