package oops.constructor;

public class ConstructorDemo2 {
	public static void main(String[] args) {
		Employee emp = new Employee("Hevin Mualni", 23);
		emp.identity();
		emp.work("Developer");
	}
}

class Person {

	int age;
	String name;

	private Person() {
		System.out.println("parent class Constructor");
	}

	public Person(String name, int age) {
		this();
		this.age = age;
		this.name = name;
	}

	void identity() {
		System.out.println("Name : " + name + " Age : " + age);
	}
}

class Employee extends Person {

	public Employee(String name, int age) {

		/*
		 * super keyword is used when child class extends parent class and
		 * called constructor of parent class in the base class.
		 */
		super(name, age);
		this.name = name;
		this.age = age;
	}

	void work(String work) {

		System.out.println("Name : " + name + " Age : " + age + " work : " + work);

	}

}