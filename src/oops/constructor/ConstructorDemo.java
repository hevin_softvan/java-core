package oops.constructor;

public class ConstructorDemo {

	public static void main(String[] args) {

		Constructor con = new Constructor();
		con.display();
		Constructor con1 = new Constructor("JAVA");
		con1.display();
	}

}

class Constructor {

	// if we can not created Constructor java is call its default constructor.

	// non paramererised Constructor or difault constructor
	public Constructor() {
		System.out.println("Default Constructor");
	}

	// parameterised Constructor
	public Constructor(String name) {

		// this keyword used called the defualt constructor in same class.
		this();
		System.out.println("name is: " + name);
	}

	void display() {
		System.out.println("how are you?");
	}

}