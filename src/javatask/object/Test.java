package javatask.object;

//cretae class
class Object {

	// set field
	int number1 = 20;
	int number2 = 30;

	// create method for summation of two number
	int sum() {

		int sum = number1 + number2;
		return sum;
	}
}

// create main class
public class Test {

	// define main method
	public static void main(String args[]) {

		// create object of class
		Object obj = new Object(); /*
									 * here we call default constructor which is
									 * createdby java itself.
									 */
		System.out.println("First output before intialize new value : " + obj.sum());

		// here we intialize new vlaue of fields.
		obj.number1 = 20;
		obj.number2 = 80;

		// console output
		System.out.println("second output after intialize new value : " + obj.sum());
	}

}
