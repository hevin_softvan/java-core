package javatask.hello;

//create a class.
public class Hello {

	// define main method
	public static void main(String[] args) {

		// create field using datatype
	    String msg = "hello world!";

		// print output in console
		System.out.println(msg);
	}

}


//output
//hello world!