package javatask.staticClassDemo;

public class StaticClass2 {

	static int number = 10;
	/*
	 * here i create variable without static keyword so call to this variable we
	 * need to crate object of class.
	 */
	int number2 = 20;

	/*
	 * same as if we create method without static keyword also need to create
	 * object to call this method.
	 */ static void sum() {

		int sum = number + number;
		System.out.println(sum);
	}

	static {
		System.out.println("Static Block");
	}

	/*
	 * here we create static method in only one class so we can not use
	 * reference of class.
	 */
	public static void main(String[] args) {

		System.out.println("Main Method");
		System.out.println(number);
		sum();

		StaticClass2 obj = new StaticClass2();
		System.out.println(obj.number2);

	}

}
