
package javatask.staticClassDemo;

class Demo {

	static int number = 10;

	static void sum() {

		int sum = number + number;
		System.out.println(sum);
	}

	static {
		System.out.println("Static Block");
	}

}

/*
 * here create other method so, if we call variable,method,or block in this
 */
public class StaticClass {

	public static void main(String[] args) {
		System.out.println("Main method");
		System.out.println(Demo.number);
		Demo.sum();

	}

}
