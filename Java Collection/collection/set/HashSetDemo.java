package collection.set;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class HashSetDemo {

	public static void main(String[] args) {
		Set<String> cars = new TreeSet<String>();
		// Set<String> cars = new LinkedHashSet<String>();
		// Set<String> cars = new HashSet<String>();
		cars.add("Verna");
		cars.add("Fortuner");
		cars.add("Creta");
		cars.add("Baleno");
		cars.add("Harrier");
		cars.add("Seltos");
		cars.add("Innova");
		cars.add("Q5");

		System.out.println(cars);

		System.out.println(cars.size());

		for (String c : cars) {
			System.out.println(c);
		}

		System.out.println(cars.remove("Q5"));
		System.out.println(cars);

		// If get elements from set then firstly set convert to List.
		List<String> carsList = new ArrayList<String>(cars);
		System.out.println(carsList.get(5));

	}
}
