package collection.arraylist;

import java.util.*;

public class ArrayListIteratorDemo {
	public static void main(String[] args) {

		ArrayList<String> cars = new ArrayList<String>(5);

		// Add an element in cars list.
		cars.add("Verna");
		cars.add("Fortuner");
		cars.add("Creta");
		cars.add("Baleno");
		cars.add("Harrier");
		cars.add("Seltos");
		cars.add("Brezza");

		// Iterate through for loop
		System.out.println("Car list iterate by for loop : ");
		for (int i = 0; i < cars.size(); i++) {

			System.out.println(cars.get(i));
		}

		System.out.println("----------------------------------------------------");

		// Iterate through forEach loop.
		System.out.println("Car list iterate by forEach loop : ");
		for (String car : cars) {
			System.out.println(car);
		}

		System.out.println("----------------------------------------------------");

		// Iterate through Iterator
		System.out.println("Car list iterate by Iterator  : ");
		Iterator<String> iterator = cars.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());

		}
		System.out.println("----------------------------------------------------");
		// Iterate through ListIterator
		System.out.println("Car list iterate by ListIterator  : ");
		ListIterator<String> listIterator = cars.listIterator();
		while (listIterator.hasNext()) {
			System.out.println(listIterator.next());

		}
		System.out.println("----------------------------------------------------");
		System.out.println("Car list iterate by ListIterator in reverse  : ");
		while (listIterator.hasPrevious()) {
			System.out.println(listIterator.previous());

		}
		System.out.println("----------------------------------------------------");

		System.out.println("----------------------------------------------------");

		// Sort By use collection
		Collections.sort(cars);
		System.out.println(cars + " :::::::: ");

		System.out.println("----------------------------------------------------");

		// Iterate through Stream api
		cars.stream().sorted().forEach(System.out::println);

		System.out.println("----------------------------------------------------");

		// Sort by name
		cars.stream().sorted(Comparator.naturalOrder()).forEach(System.out::println);

		System.out.println("----------------------------------------------------");

		// Sort by name in reverse order
		cars.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);

		System.out.println("----------------------------------------------------");

		// Use filter for find Fortuner element.
		cars.stream().filter(f -> f.equals("Fortuner")).forEach(System.out::println);

		System.out.println("----------------------------------------------------");

		// Use filter for find Fortuner element.
		cars.stream().filter(f -> f.startsWith("B")).forEach(System.out::println);

	}
}
