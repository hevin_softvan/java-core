package collection.arraylist;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {

		// Declaring ArrayList with size Defining.
		// Here 5 is initial capacity of cars list.
		ArrayList<String> cars = new ArrayList<>(5);

		// Add an element in cars list.
		cars.add("Verna");
		cars.add("Fortuner");
		cars.add("Creta");
		cars.add("Baleno");
		cars.add("Harrier");
		cars.add("Seltos");
		cars.add("Innova");
		cars.add("Q5");

		// here output of arraylist of cars
		System.out.println("List of cars : " +cars);

		// Add null or duplicate value multiple time.
		cars.add(null);
		cars.add("Creta");
		cars.add("Baleno");
		cars.add(null);

		// Output of ArrayList of cars with null and duplicate value.
		System.out.println("List of cars after adding null or duplicate values : " + cars);

		/*
		 * When removing an element middle of the list. element on this position
		 * is shift to one position left.
		 */
		// Removing elements of list by index.
		cars.remove(8);
		cars.remove(10);

		/*
		 * If remove by object name and there are multiple objects name then it
		 * remove first occurance of this object name.
		 */
		// Removing elements of list by Object.
		cars.remove("Creta");
		System.out.println("List of cars after remove elements  : " +cars);

		// Get values by index.
		System.out.println("Get element by index : " +cars.get(5)); // Innova

		/*
		 * When adding an element middle of the list. element on this position
		 * is shift to one position right.
		 */
		// Adding by index.
		cars.add(8, "Brezza");
		System.out.println("Add element by index : " +cars);

		// Get the position of Harrier in cars list.
		System.out.println("Get the position of Harrier in cars list : " +cars.indexOf("Harrier")); //3

		//Check the list is empty or not?
		System.out.println(cars.isEmpty()); //false if not empty else return true.
		
		//Check the element in list or not.
		System.out.println(cars.contains("Fortuner")); //false if not contain else return true.
		
		//Find the size of ArrayList
		System.out.println(cars.size());
	}

}
