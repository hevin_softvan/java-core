package collection.map;

import java.security.Key;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import java.util.TreeMap;

import org.omg.CORBA.portable.ValueBase;

public class MapDemo {
	public static void main(String[] args) {

		Map<String, Integer> player = new TreeMap<String, Integer>();
		player.put("Rohit", 45);
		player.put("Kohli", 18);
		player.put("Dhoni", 07);
		player.put("Jadeja", 12);
		player.put("Rahul", 01);
		player.put("Raina", 05);

		System.out.println(player);

		for (Map.Entry<String, Integer> map : player.entrySet()) {
			System.out.println(map.getKey() + "  : " + map.getValue());
			System.out.println(map.getKey());
			System.out.println(map.getValue());
		}

		System.out.println(">>>" + player.get("Rahul"));

		player.putIfAbsent("Ashwin", 03);
		System.out.println(player);

		player.entrySet().stream().sorted(Comparator.comparing(p -> p.getValue()))
				.filter(f -> f.getKey().startsWith("R")).forEach(System.out::println);

		Map<String, Integer> player1 = new HashMap<>();
		player1.put("Rohit", 45);
		player1.put("Kohli", 18);
		player1.put("Dhoni", 07);
		player1.put("Jadeja", 12);
		player1.put("Rahul", 01);
		player1.put("Raina", 05);

		System.out.println(player1);

	}

}
