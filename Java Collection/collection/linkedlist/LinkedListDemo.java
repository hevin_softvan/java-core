package collection.linkedlist;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {

		// Create LinkedList of cars list
		LinkedList<String> cars = new LinkedList<String>();
		cars.add("Verna");
		cars.add("Fortuner");
		cars.add("Creta");
		cars.add("Baleno");
		cars.add("Harrier");
		cars.add("Seltos");
		cars.add("Innova");
		cars.add("Q5");

		// print list of cars
		System.out.println("List of cars : " + cars);

		// Add car from head : addFirst(e), offerFirst(e)
		cars.addFirst("Wagnor");
		System.out.println("List of cars after adding from head : " + cars);

		cars.offerFirst("Altroz");
		System.out.println("List of cars after adding from head : " + cars);

		// Add car from tail : add(e), addLast(e)
		cars.offer("Amaze");
		System.out.println("List of cars after adding from tail : " + cars);

		cars.addLast("Swift");
		System.out.println("List of cars after adding from tail : " + cars);

		// Remove car from head : poll(), pollFitrst(), remove(), removeFirst()
		cars.remove();
		System.out.println("List of cars after remove from head : " + cars);
		cars.poll();
		System.out.println("List of cars after remove from head : " + cars);

		// Remove car from tail : pollLast(), removeLast()
		cars.removeLast();
		System.out.println("List of cars after remove from tail : " + cars);
		cars.pollLast();
		System.out.println("List of cars after remove from tail : " + cars);

		// Remove by index
		cars.remove(4);
		System.out.println("List of cars after remove by index : " + cars);

		// Remove by object
		cars.remove("Creta");
		System.out.println("List of cars after remove by Object : " + cars);

		// get data from head : peek(), element(), peekFirst(), getFirst().

		System.out.println("get elements from head : " + cars.peek());
		System.out.println("get elements from head : " + cars.element());
		System.out.println("get elements from head : " + cars.peekFirst());
		System.out.println("get elements from head : " + cars.getFirst());

		// get data from tail : getLast(), peekLast();
		System.out.println("get elements from tail : " + cars.peekLast());
		System.out.println("get elements from tail : " + cars.getLast());

		// get element by index
		System.out.println("get elements by index : " + cars.get(4));
	}
}
