package collection.stack;

import java.util.Stack;

//Stack : Last in first out (LIFO).
public class StackDemo {

	public static void main(String[] args) {

		Stack<String> stack = new Stack<String>();
		stack.push("Creta");
		stack.push("Swift");
		stack.push("Volvo");
		stack.push("Q5");

		System.out.println(stack);

		System.out.println(stack.pop());

		System.out.println(stack.peek());
	}
}