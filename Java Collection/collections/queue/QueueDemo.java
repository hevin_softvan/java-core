package collections.queue;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

//Queue : First in first out (FIFO).
public class QueueDemo {

	public static void main(String[] args) {

		Queue<String> queue = new LinkedList<String>();

		queue.offer("Creta");
		queue.offer("Swift");
		queue.offer("Wagnor");
		queue.offer("Thar");
		queue.offer("Q5");

		System.out.println(queue);

		System.out.println(queue.poll());

		System.out.println(queue.peek());

	

	}
}
