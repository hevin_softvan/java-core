package collections.queue;

import java.time.format.ResolverStyle;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueDemo {

	public static void main(String[] args) {
		Queue<String> pq = new PriorityQueue<String>(Comparator.reverseOrder());
		pq.offer("Creta");
		pq.offer("Wagnor");
		pq.offer("Swift");
		pq.offer("Volvo");
		pq.offer("Amaze");

		System.out.println(pq);
		System.out.println(pq.poll());

	}

}
