package collections.queue;

import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeDemo {

	public static void main(String[] args) {

		Deque<String> q = new ArrayDeque<String>();
		q.offer("Creta");
		q.offer("Wagnor");
		q.offer("Swift");
		q.offerFirst("Volvo");
		q.offerLast("Q5");

		System.out.println(q);

		System.out.println(q.remove()); // volvo
		System.out.println(q.removeFirst());// creta
		System.out.println(q.removeLast()); // Q5

		System.out.println(q.peek()); // Volvo
		System.out.println(q.peekFirst()); // volvo
		System.out.println(q.peekLast()); // Q5

	}
}
