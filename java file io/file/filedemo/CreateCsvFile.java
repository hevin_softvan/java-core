package file.filedemo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class CreateCsvFile {

	public static void main(String[] args) throws IOException {
		WriteCsv writeCsv = new WriteCsv();
		SearchCsv searchCsv = new SearchCsv();

		Scanner sc = new Scanner(System.in);
		System.out.println("1 : Register user :");
		System.out.println("2 : search by name :");

		int select = sc.nextInt();

		if (select == 1) {
			writeCsv.write();
		} else if (select == 2) {
			searchCsv.search();
		} else {
			System.out.println("Exit");
		}
		//
		// File file = new File("User.csv");
		// boolean f = file.createNewFile();
		// System.out.println("Crete new csv file : " + f);

	}

}

class WriteCsv {

	void write() throws IOException {

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bfr = new BufferedReader(isr);

		FileWriter fileWriter = new FileWriter("User.csv", true);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		System.out.print("Enter your name : ");
		String name = bfr.readLine();

		System.out.print("Enter your Lastname : ");
		String lname = bfr.readLine();

		System.out.print("Enter your Graduation : ");
		String garduate = bfr.readLine();

		bufferedWriter.write(name);
		bufferedWriter.write(",");
		bufferedWriter.write(lname);
		bufferedWriter.write(",");
		bufferedWriter.write(garduate);
		bufferedWriter.write("\n");

		bufferedWriter.close();
		fileWriter.close();
		bfr.close();
		isr.close();
	}
}

class SearchCsv {

	public void search() throws IOException {

		InputStreamReader isr1 = new InputStreamReader(System.in);
		BufferedReader bfr1 = new BufferedReader(isr1);

		FileReader fr = new FileReader("User.csv");
		BufferedReader bfw1 = new BufferedReader(fr);

		System.out.print("Search your result : ");
		String name = bfr1.readLine();

		String line;
		boolean result = true;
		while ((line = bfw1.readLine()) != null) {

			// F System.out.println(line);
			String word[] = line.split(" ");

			for (int i = 0; i < word.length; i++) {

				if (word[i].contains(name)) {

					System.out.println("your finding result is : " + result);
					System.out.println(line);
					System.out.println("*********************************************************************");

				}
			}

		}

		bfw1.close();
		fr.close();
		bfr1.close();
		isr1.close();

	}
}
