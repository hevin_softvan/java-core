package file.filedemo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class UserInput {

	public static void main(String[] args) throws IOException {

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bfr = new BufferedReader(isr);

		System.out.println("What is your name?");
		String name = bfr.readLine();

		// System.out.println("Name is : " + name);

		FileReader fileReader = new FileReader("javatask.txt");
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		String lineRead;
		int wordCount = 0;

		while ((lineRead = bufferedReader.readLine()) != null) {

			System.out.println(lineRead);

			String word[] = lineRead.split(" ");

			for (int i = 0; i < word.length; i++) {

				if (word[i].contains(name)) {
					wordCount++;

				}

			}

		}
		System.out.println("total word is : " + wordCount);

	}

}
