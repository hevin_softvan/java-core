package file.filedemo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReadAndWrite {

	public static void main(String[] args) throws IOException {
		WriteFile writeFile = new WriteFile();
		writeFile.write();

		ReadFile readFile = new ReadFile();
		readFile.read();

	}

}

// for write file
class WriteFile {
	public void write() throws IOException {

		FileWriter fileWriter = new FileWriter("javatask.txt");
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		bufferedWriter.write("Java is object oriented langauge.");
		bufferedWriter.newLine();

		bufferedWriter.write("Java is a popular programming language, created in 1995.");
		bufferedWriter.newLine();

		bufferedWriter.write(
				"Java is an object oriented language which gives a clear structure to programs and allows code to be reused, lowering development costs");
		bufferedWriter.newLine();

		bufferedWriter.close();
		fileWriter.close();

	}

}

// for read file
class ReadFile {

	public void read() throws IOException {

		FileReader fileReader = new FileReader("javatask.txt");
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		String lineRead;
		int line = 0;

		int wordCount = 0;

		int charCount = 0;

		while ((lineRead = bufferedReader.readLine()) != null) {

			System.out.println(lineRead);

			String word[] = lineRead.split(" ");

			for (int i = 0; i < word.length; i++) {
				if (word[i].trim().length() > 0) {
					charCount += word[i].length();
					wordCount++;
				}
			}

			line++;

		}
		System.out.println("Total line is : " + line);
		System.out.println("Total word is : " + wordCount);
		System.out.println("Total character is : " + charCount);

		bufferedReader.close();
		fileReader.close();

	}

}
