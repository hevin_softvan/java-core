package file.filedemo;

import java.io.File;
import java.io.IOException;

public class CreateFile {
	public static void main(String[] args) throws IOException {
		NewFile nf = new NewFile();
		nf.createFile();
	}

}

class NewFile {

	public void createFile() throws IOException {

		// createfile
		File file = new File("JavaTask.txt");
		boolean create = file.createNewFile();
		System.out.println("File is created : " + create);

		if (create) {
			System.out.println("created Successfully");
		} else {
			System.out.println("not successfully");
		}

		System.out.println("File name : " + file.getName());

		System.out.println(file.canRead());

		System.out.println(file.canWrite());

		System.out.print("file path : " + file.getAbsolutePath());

		File file3 = new File("javaTaskInno");
		file3.mkdirs();

		// renameFile
		File renameFile = new File("javatask.txt");
		boolean file2 = file.renameTo(renameFile);
		System.out.println(file2);

		if (file2) {
			System.out.println("rename Successfully");
		} else {
			System.out.println("not successfully");
		}

		System.out.println(renameFile.getName());

		System.out.println("__________________________________________________");

		String listDirt = "M:\\javaWorkspace\\softvan\\java task";
		File listDir = new File(listDirt);
		String[] path = listDir.list();
		for (String dirNames : path) {
			System.out.println(dirNames);
		}
	}
}