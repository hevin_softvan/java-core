package exception.throwdemo;

import java.util.Scanner;

public class ThrowDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your age.");
		int age = sc.nextInt();
		Voter vtr = new Voter();
		vtr.checkAge(age);
		sc.close();
	}
}

// create custom error.
class CustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CustomException(String message) {
		super(message);

	}
}

class Voter {

	public void checkAge(int age) {

		try {
			if (age >= 18) {
				System.out.println("you are eligible for voting.");
			} else {

				throw new CustomException("you are not eligible for voting until you are not eighteen.");
			}
		} catch (CustomException e) {
			System.out.println(e.getMessage());
		}

	}
}