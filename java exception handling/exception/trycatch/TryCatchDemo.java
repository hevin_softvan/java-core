package exception.trycatch;

public class TryCatchDemo {

	public static void main(String[] args) {
		Demo d = new Demo();
		d.demo();

	}

}

class Demo {

	void demo() {

		int[] numberList = { 1, 3, 5, 8, 9, 10 };
		try {

			// It throws arithmatic exception
			int num = numberList[5] / 0;
			System.out.println(num);

			// It throws ArrayIndexOutOfBound Exception.
			System.out.println(numberList[9]);

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("no element found.");

		} catch (ArithmeticException e) {
			System.out.println("can  not divided by zero.");

		} catch (Exception e) {
			System.out.println(e);

		} finally {
			System.out.println("excepion handled successfully.");
		}
	}

}
